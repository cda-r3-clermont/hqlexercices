package com.hibernate;

import com.hibernate.model.Adresse;
import com.hibernate.model.Utilisateur;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Objects;

public class Select {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        // Selection de tous les enregistrements
        List<Adresse> adresses;
        adresses = session.createQuery("FROM Adresse", Adresse.class).getResultList();

        for (Adresse adresse: adresses){
            System.out.println(adresse);
        }

        List<Utilisateur> users;
        users = session.createQuery("FROM Utilisateur", Utilisateur.class).getResultList();


        List<Object[]> rueCodePostal;
        rueCodePostal = session.createQuery(
                "SELECT a.rue, a.codePostal FROM Adresse a",
                        Object[].class)
                .getResultList();



        tx.commit();
        sf.close();
    }
}

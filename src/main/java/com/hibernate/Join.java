package com.hibernate;

import com.hibernate.model.Utilisateur;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class Join {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        // Selection avec une jointure interne
        // (l'utilisateur Aurélien n'est pas retourné puisqu'il n'a pas d'adresse)
        List<Utilisateur> usersJoinInner =
                session.createQuery(
                        "SELECT u FROM Utilisateur u INNER JOIN u.adresse"
                                , Utilisateur.class)
                .getResultList();





        // Selection avec une jointure externe
        // (Si un utilisateur existe mais qu'il n'y a pas de correspondance dans Adresse il est retourné)
        List<Utilisateur> usersJoinLeft =
                session.createQuery(
                        "SELECT u.email FROM Utilisateur u LEFT JOIN u.adresse ",
                                Utilisateur.class)
                        .getResultList();

        // par la droite
        List<Utilisateur> usersJoinRight =
                session.createQuery(
                        "SELECT u FROM Utilisateur u RIGHT JOIN u.adresse ",
                                Utilisateur.class)
                        .getResultList();


        tx.commit();
        session.close();
        sf.close();

    }



}

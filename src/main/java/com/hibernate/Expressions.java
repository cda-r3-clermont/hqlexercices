package com.hibernate;

import com.hibernate.model.Adresse;
import com.hibernate.model.Utilisateur;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.sql.Array;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class Expressions {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        GregorianCalendar startDate = new GregorianCalendar(1980,1, 1);
        GregorianCalendar endDate = new GregorianCalendar(1989, 12, 31);
        // Selectionne les utilisateurs nées dans les années 80
          List<Utilisateur> usersEighties =
                session.createQuery(
                                "SELECT u FROM Utilisateur u " +
                                        "WHERE u.dateNaissance " +
                                        "BETWEEN :stDate AND :endDate"
                                , Utilisateur.class)
                        .setParameter("stDate", startDate)
                        .setParameter("endDate", endDate)
                        .getResultList();

          ArrayList<String> southCity = new ArrayList<String>();
          southCity.add("Bordeaux");
          southCity.add("Marseille");
          southCity.add("Lyon");

          // Selectionne toutes les adresses qui ne sont pas dans le sud
          List<Adresse> notSouthAdress =
                  session.createQuery("SELECT a FROM Adresse a WHERE a.ville " +
                                  "NOT IN (:array)", Adresse.class)
                                  .setParameter("array", southCity).getResultList();

         // Selectionne toutes les adresses qui sont dans le sud
        List<Adresse> southAdress =
                session.createQuery("SELECT a FROM Adresse a WHERE a.ville " +
                                "IN (:array)", Adresse.class)
                        .setParameter("array", southCity).getResultList();

        // Selectionne toutes les adresses qui sont dans le puy de dôme
        String param = "63%";
        List<Adresse> pddAdress =
                session.createQuery("SELECT a FROM Adresse a WHERE a.codePostal " +
                                "LIKE :param", Adresse.class)
                        .setParameter("param", param).getResultList();

        
        System.out.println("Hello World !");

    }
}

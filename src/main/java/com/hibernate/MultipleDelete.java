package com.hibernate;

import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MultipleDelete {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();



        session.createQuery("DELETE FROM Product p WHERE p.prix < 8")
                .executeUpdate();



        tx.commit();
        session.close();
        sf.close();

    }
}

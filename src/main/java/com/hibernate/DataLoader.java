package com.hibernate;

import com.hibernate.model.Adresse;
import com.hibernate.model.Product;
import com.hibernate.model.Utilisateur;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.time.LocalDate;
import java.util.Date;
import java.util.GregorianCalendar;

public class DataLoader {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();

        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        session.createQuery("DELETE Adresse ").executeUpdate();
        session.createQuery("DELETE Utilisateur ").executeUpdate();
        session.createQuery("DELETE Product ").executeUpdate();

        Utilisateur utilisateur1 = new Utilisateur("John", "Doe",
              new GregorianCalendar(1990,5,15), "john.doe@example.com", "hashed_password_1", new GregorianCalendar(2023,5,15), true);


        Utilisateur utilisateur2 = new Utilisateur("Alice", "Smith",
                new GregorianCalendar(1985,11,25), "alice.smith@example.com", "hashed_password_2", new GregorianCalendar(2020,10,21), false);

        Utilisateur utilisateur3 = new Utilisateur("Bob", "Johnson",
                new GregorianCalendar(1995,3,10), "bob.johnson@example.com", "hashed_password_3", new GregorianCalendar(2010,8,24), true);

        Utilisateur utilisateur4 = new Utilisateur("Emma", "Williams",
                new GregorianCalendar(1988,9,20), "emma.williams@example.com", "hashed_password_4", new GregorianCalendar(2021,11,3), false);

        Utilisateur utilisateurSansAdresse = new Utilisateur("Aurélien", "Delorme",
                new GregorianCalendar(1988,9,20), "aureliendelorme1@gmail.com", "hashed_password_5", new GregorianCalendar(2022,12,31), true);

        Utilisateur pascalGastien = new Utilisateur("Pascal", "Gastien",
                new GregorianCalendar(1963,12,2), "pgastien@clermont-foot.com", "hashed_password_6", new GregorianCalendar(1999,11,2), false);

        Utilisateur aurelienRougerie = new Utilisateur("Aurélien", "Rougerie",
                new GregorianCalendar(1963,12,2), "arougerie@asm.com", "hashed_password_6", new GregorianCalendar(2022,12,31), true);

        Utilisateur userSansAdresse = new Utilisateur("Sans", "Adresse",new GregorianCalendar(2022,12,31), "sansadresse@outlook.com", "hash_password",new GregorianCalendar(2022,12,31),false);

        session.persist(userSansAdresse);
        Adresse adressePascal = new Adresse("Rue Robert Lemoy", "Clermont-Ferrand", "63100", pascalGastien);
        Adresse adresseRougerie = new Adresse("35 Rue du Clos Four", "Clermont-Ferrand", "63100", aurelienRougerie);


        Adresse adresse1 = new Adresse("123 Rue de la Liberté", "Paris", "75001", utilisateur1);

        Adresse adresse2 = new Adresse("456 Avenue des Fleurs", "Lyon", "69002", utilisateur2);

        Adresse adresse3 = new Adresse("789 Rue du Commerce", "Marseille", "13001", utilisateur3);

        Adresse adresse4 = new Adresse("987 Avenue du Soleil", "Bordeaux", "33000", utilisateur4);

        session.persist(utilisateur1);
        session.persist(utilisateur2);
        session.persist(utilisateur3);
        session.persist(utilisateur4);
        session.persist(utilisateurSansAdresse);
        session.persist(pascalGastien);
        session.persist(aurelienRougerie);

        session.persist(adresse1);
        session.persist(adresse2);
        session.persist(adresse3);
        session.persist(adresse4);
        session.persist(adressePascal);
        session.persist(adresseRougerie);

        Product fiveTenders = new Product();
        fiveTenders.setImage("https://static.kfc.fr/images/items/lg/Menu_Tenders_5.jpg?v=g9aQxg");
        fiveTenders.setLibelle("Menu 5 tenders");
        fiveTenders.setDescription("Juste ce qu’il faut de Tenders® dans votre menu Bucket KFC : 5 Tenders®, des morceaux de poulet tendres, marinés et panés… mais surtout tellement croustillants ! + 2 sauces.");
        fiveTenders.setPrix(5.95);

        Product menuTower = new Product();
        menuTower.setDescription("Le Tower Cheese & Bacon, c’est la recette parfaite. Entre 2 tranches de pain aux graines de sésame : un filet de poulet Original mariné et pané, un Rösti (galette de pomme de terre), une tranche de bacon, deux tranches de cheddar fondant et une touche de sauce moutarde US et de ketchup. Que demander de plus ?");
        menuTower.setImage("https://static.kfc.fr/images/items/lg/Tower_CB_Menu-FR.jpg?v=3mbkAL");
        menuTower.setPrix(13.45);
        menuTower.setLibelle("Menu Tower® Cheese & Bacon");


        Product menuBoxMaster = new Product();
        menuBoxMaster.setDescription("Retrouvez dans votre menu wrap KFC : 1 Boxmaster® Original, la rencontre ultime entre un filet de poulet Original mariné et pané, son Rösti (galette de pomme de terre), une tranche de cheddar fondu, de la salade, un peu de tomate, et une fine galette de blé.");
        menuBoxMaster.setImage("https://static.kfc.fr/images/items/lg/BoxmasterOR-Menu.jpg?v=3mbkAL");
        menuBoxMaster.setPrix(11.15);
        menuBoxMaster.setLibelle("Menu Boxmaster® Original");

        session.save(menuTower);
        session.save(menuBoxMaster);
        session.save(fiveTenders);

        tx.commit();
        session.close();


        sf.close();
    }
}

package com.hibernate;

import com.hibernate.model.Adresse;
import com.hibernate.model.Product;
import com.hibernate.model.Utilisateur;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.sql.Array;
import java.util.*;

public class Main {

        public static void main(String[] args) {
            SessionFactory sf = new HibernateUtil().buildSessionFactory();
            Session session = sf.getCurrentSession();
            Transaction tx = session.beginTransaction();


            tx.commit();
            session.close();
            sf.close();

        }
}

package com.hibernate;

import com.hibernate.model.Utilisateur;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrderByGroupBy {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        List<String> firstnames;

        firstnames = session.createQuery(
                "SELECT u.prenom FROM Utilisateur u ORDER BY u.dateNaissance ASC",
                String.class).getResultList();

        for (String firstname: firstnames){
            System.out.println(firstname);
        }

        ArrayList<Object[]> userByCity =
                (ArrayList<Object[]>) session.createQuery(
                       "SELECT a.codePostal ,COUNT(u)  " +
                               "FROM Utilisateur u " +
                               "INNER JOIN u.adresse a " +
                               "GROUP BY a.codePostal").getResultList();

        for(Object[] userCity: userByCity){
            System.out.println(userCity[0] + "-"+userCity[1]);
        }


        tx.commit();
        session.close();

        // Utilisateurs du plus jeune au plus agé


        sf.close();

    }
}

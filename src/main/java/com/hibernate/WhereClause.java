package com.hibernate;

import com.hibernate.model.Adresse;
import com.hibernate.model.Product;
import com.hibernate.model.Utilisateur;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class WhereClause {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        // Les adresses à Clermont-Fd
        List<Adresse> adresses;
        adresses = session.createQuery(
                "FROM Adresse a WHERE a.codePostal = '63000' OR a.codePostal = '63100'", Adresse.class)
                .getResultList();

        // Les produits avec un prix supérieur à 10 euros
        List<Product> products;
        products = session.createQuery("FROM Product p WHERE p.prix > 10", Product.class).getResultList();


        // Les utilisateurs ayant le code postal 63100 ET une adresse vérifiée
        List<Utilisateur> users;
        users = session.createQuery(
                        "SELECT u FROM Utilisateur  u " +
                                "LEFT JOIN u.adresse a " +
                                "WHERE a.codePostal = '63100' AND u.emailVerifie = true ",
                        Utilisateur.class)
                .getResultList();



        tx.commit();
        sf.close();
    }
}

package com.hibernate.model;



import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "utilisateur")
public class Utilisateur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Basic
    private String prenom;

    @Basic
    private String nom;

    @Column(name = "date_naissance")
    @Temporal(TemporalType.DATE)
    private Calendar dateNaissance;

    @Column(name = "derniere_connexion")
    @Temporal(TemporalType.DATE)
    private Calendar derniereConnexion;

    @Basic
    private String email;

    @Column(name = "mot_de_passe")
    private String motDePasse;

    @Column(name = "email_verifie")
    private boolean emailVerifie;

    @OneToOne(mappedBy = "utilisateur")
    @Cascade(CascadeType.DELETE)
    private Adresse adresse;

    public Utilisateur() {
    }

    public Utilisateur(String prenom, String nom, Calendar dateNaissance, String email, String motDePasse, Calendar derniereConnxion, boolean emailVerifie) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.email = email;
        this.motDePasse = motDePasse;
        this.derniereConnexion = derniereConnxion;
        this.emailVerifie = emailVerifie;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Calendar getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Calendar dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }


    public Calendar getDerniereConnexion() {
        return derniereConnexion;
    }

    public void setDerniereConnexion(Calendar derniereConnexion) {
        this.derniereConnexion = derniereConnexion;
    }

    public boolean isEmailVerifie() {
        return emailVerifie;
    }

    public void setEmailVerifie(boolean emailVerifie) {
        this.emailVerifie = emailVerifie;
    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "id=" + id +
                ", prenom='" + prenom + '\'' +
                ", nom='" + nom + '\'' +
                ", dateNaissance=" + dateNaissance +
                ", derniereConnexion=" + derniereConnexion +
                ", email='" + email + '\'' +
                ", motDePasse='" + motDePasse + '\'' +
                ", emailVerifie=" + emailVerifie +
                '}';
    }
}

package com.hibernate;

import com.hibernate.model.Adresse;
import com.hibernate.model.Product;
import com.hibernate.utils.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Aggregation {
    public static void main(String[] args) {
        SessionFactory sf = new HibernateUtil().buildSessionFactory();
        Session session = sf.getCurrentSession();
        Transaction tx = session.beginTransaction();

        // Moyenne des prix du KFC
        Double moyennePrixKfc =
                session.createQuery(
                        "SELECT AVG(p.prix) FROM Product p", Double.class).getSingleResult();

        System.out.println("Moyenne des prix du KFC : " + moyennePrixKfc);

        // Prix d'un menu Tower avec un menu 5 tenders
        Double prixTowerTender =
                (Double) session.createQuery(
                        "SELECT SUM(p.prix) " +
                                "FROM Product p " +
                                "WHERE p.libelle = 'Menu Boxmaster® Original' " +
                                "OR p.libelle = 'Menu 5 tenders'").getSingleResult();

        System.out.println("Prix total de la commande : "+prixTowerTender);


        Double produitLePlusCher =
                (Double) session.createQuery(
                        "SELECT MAX(p.prix) as price " +
                                "FROM Product p").getSingleResult();

        System.out.println("Le menu le plus cher coute :"+ produitLePlusCher);

        Double produitLeMoinsCher =
                (Double) session.createQuery(
                        "SELECT MIN(p.prix) as price " +
                                "FROM Product p").getSingleResult();

        System.out.println("Le menu le moins cher coute :"+ produitLeMoinsCher);

        Long countProduct =
                (Long) session.createQuery(
                        "SELECT COUNT (p) as nbProduct " +
                                "FROM Product p").getSingleResult();

        System.out.println(countProduct + " produits sont référencés à la carte");

        tx.commit();
        session.close();
        sf.close();
    }
}
